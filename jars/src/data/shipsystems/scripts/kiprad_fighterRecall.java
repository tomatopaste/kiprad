package data.shipsystems.scripts;

import com.fs.starfarer.api.Global;
import com.fs.starfarer.api.combat.MutableShipStatsAPI;
import com.fs.starfarer.api.combat.ShipAPI;
import com.fs.starfarer.api.combat.WeaponAPI;
import com.fs.starfarer.api.impl.combat.BaseShipSystemScript;
import com.fs.starfarer.api.util.Misc;
import java.awt.*;
import java.util.List;
import java.util.ArrayList;
import java.util.EnumSet;

import static com.fs.starfarer.api.impl.combat.RecallDeviceStats.JITTER_COLOR;
import static com.fs.starfarer.api.impl.combat.RecallDeviceStats.getFighters;

public class kiprad_fighterRecall extends BaseShipSystemScript{
    public static final float WEAPON_FLUX_COST_MOD = 0.5f;
    public static final float FIGHTER_RANGE_MOD = 0.1f;

    public static final Object KEY_JITTER = new Object();
    public static final Color JITTER_UNDER_COLOUR = new Color(127,50,127,125);
    public static final Color JITTER_COLOUR = new Color(127,50,127,75);

    @Override
    public void apply(MutableShipStatsAPI stats, String id, State state, float effectLevel) {

        ShipAPI ship = null;
        if (stats.getEntity() instanceof ShipAPI) {
            ship = (ShipAPI) stats.getEntity();
        } else {
            return;
        }

        if (effectLevel > 0) {
            float jitterLevel = effectLevel;
            //you thought this was a range bonus -- BUT IT WAS ME, DIO!
            float maxRangeLoss = 5f;
            float jitterRangeLoss = jitterLevel * maxRangeLoss;
            for (ShipAPI fighter : getFighters(ship)) {
                if (fighter.isHulk()) continue;

                if (jitterLevel > 0) {
                    fighter.setWeaponGlow(effectLevel, Misc.setAlpha(JITTER_UNDER_COLOUR, 255), EnumSet.allOf(WeaponAPI.WeaponType.class));

                    fighter.setJitterUnder(KEY_JITTER, JITTER_COLOUR, jitterLevel, 5, 0f, jitterRangeLoss);
                    fighter.setJitter(KEY_JITTER, JITTER_UNDER_COLOUR, jitterLevel, 2, 0f, 0 + jitterRangeLoss * 1f);
                    Global.getSoundPlayer().playLoop("system_targeting_feed_loop", ship, 1f, 1f, fighter.getLocation(), fighter.getVelocity());
                }
            }
        }

        ship.getMutableStats().getEnergyWeaponFluxCostMod().modifyMult(id, WEAPON_FLUX_COST_MOD);
        ship.getMutableStats().getBallisticWeaponFluxCostMod().modifyMult(id, WEAPON_FLUX_COST_MOD);

        ship.getMutableStats().getFighterWingRange().modifyMult(id, FIGHTER_RANGE_MOD);

    }

    private List<ShipAPI> getFighters(ShipAPI carrier) {
        List<ShipAPI> result = new ArrayList<ShipAPI>();

        for (ShipAPI ship : Global.getCombatEngine().getShips()) {
            if (!ship.isFighter()) continue;
            if (ship.getWing() == null) continue;
            if (ship.getWing().getSourceShip() == carrier) {
                result.add(ship);
            }
        }

        return result;
    }

    @Override
    public void unapply(MutableShipStatsAPI stats, String id) {
        ShipAPI ship = null;
        if (stats.getEntity() instanceof ShipAPI) {
            ship = (ShipAPI) stats.getEntity();
        } else {
            return;
        }
        for (ShipAPI fighter : getFighters(ship)) {
            if (fighter.isHulk()) continue;
            fighter.setJitterUnder(KEY_JITTER, JITTER_COLOR, 1f, 5, 0f, 5f);
            fighter.setJitter(KEY_JITTER, JITTER_UNDER_COLOUR, 1f, 2, 0f, 5f);
        }

        stats.getEnergyWeaponFluxCostMod().unmodify();
        stats.getBallisticWeaponFluxCostMod().unmodify();

        stats.getFighterWingRange().unmodify();

    }

    @Override
    public StatusData getStatusData(int index, State state, float effectLevel) {

        if (index == 0) {
            return new StatusData("Weapon Flux Cost Halved", false);
        }

        if (index == 1) {
            return new StatusData("Fighter Wings Recalled", true);
        }
        return null;
    }
}
