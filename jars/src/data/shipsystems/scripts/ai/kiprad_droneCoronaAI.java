package data.shipsystems.scripts.ai;

import com.fs.starfarer.api.combat.*;
import com.fs.starfarer.api.util.IntervalUtil;
import org.lazywizard.lazylib.MathUtils;
import org.lwjgl.util.vector.Vector2f;

public class kiprad_droneCoronaAI implements ShipSystemAIScript {

    private ShipAPI ship;
    private CombatEngineAPI engine;
    private ShipwideAIFlags flags;
    private ShipSystemAPI system;

    private IntervalUtil tracker = new IntervalUtil(0.5f, 1f);

    private boolean isShipInWeaponEngagementRange = false;
    private boolean isShipInWingNotWeaponEngagementRange = false;
    private boolean isFluxAboveThreshold = false;
    private float distanceToTarget = 0f;
    private float longestWeaponRange = 0f;
    private float longestWingRange = 0f;
    private float totalWeaponFluxPerSecond = 0f;

    public void init(ShipAPI ship, ShipSystemAPI system, ShipwideAIFlags flags, CombatEngineAPI engine) {
        this.ship = ship;
        this.flags = flags;
        this.engine = engine;
        this.system = system;

        for (WeaponAPI weapon : ship.getAllWeapons()) {
            //gets the range of the longest range ballistic or energy weapon on init
            if (weapon.getRange() > longestWeaponRange) {
                longestWeaponRange = weapon.getRange();
            }

            totalWeaponFluxPerSecond += weapon.getDerivedStats().getFluxPerSecond();
        }

        //gets longest fighter wing range
        for (FighterWingAPI wing : ship.getAllWings()) {
            if (wing.getRange() > longestWingRange) {
                longestWingRange = wing.getRange();
            }
        }
    }

    private final float FLUX_CRITICAL_THRESHOLD = 0.75f;
    private final float WEAPON_FLUX_GREATER_THAN_DISSIPATION_THRESHOLD = 0.5f;

    @SuppressWarnings("unchecked")
    public void advance(float amount, Vector2f missileDangerDir, Vector2f collisionDangerDir, ShipAPI target) {
        tracker.advance(amount);

        if (tracker.intervalElapsed()  && ship != null) {
            //get current ship flux level
            float fluxLevel = ship.getFluxTracker().getFluxLevel();

            //determines the distance to the ship's target
            if (target != null) {
                distanceToTarget = MathUtils.getDistance(ship, target);
                //is target in range of weapons or fighters
                isShipInWeaponEngagementRange = (longestWeaponRange > distanceToTarget);
                isShipInWingNotWeaponEngagementRange = (!isShipInWeaponEngagementRange && longestWingRange > distanceToTarget);
            } else {
                isShipInWeaponEngagementRange = false;
                isShipInWingNotWeaponEngagementRange = false;
            }

            //determines if threshold is reached, always above CRITICAL flux or if weapon flux/sec is greater than total dissipation then checks if above a lower amount
            if (fluxLevel > FLUX_CRITICAL_THRESHOLD) {
                isFluxAboveThreshold = true;
            } else if (totalWeaponFluxPerSecond > ship.getMutableStats().getFluxDissipation().getBaseValue() && fluxLevel > WEAPON_FLUX_GREATER_THAN_DISSIPATION_THRESHOLD) {
                isFluxAboveThreshold = true;
            } else {
                isFluxAboveThreshold = false;
            }

            if (!system.isOn() && isFluxAboveThreshold && isShipInWeaponEngagementRange && !isShipInWingNotWeaponEngagementRange && target != null) {
                //turn on system
                ship.useSystem();
                return;
            } else if (system.isOn() && (isShipInWingNotWeaponEngagementRange || !isShipInWeaponEngagementRange || !isFluxAboveThreshold)) {
                //turn off system
                ship.useSystem();
                return;
            }
        }
    }
}