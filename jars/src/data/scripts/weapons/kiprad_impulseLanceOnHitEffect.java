package data.scripts.weapons;

import com.fs.starfarer.api.campaign.AsteroidAPI;
import com.fs.starfarer.api.combat.*;
import com.fs.starfarer.combat.entities.terrain.Asteroid;
import org.lazywizard.lazylib.MathUtils;
import org.lazywizard.lazylib.VectorUtils;
import org.lazywizard.lazylib.combat.CombatUtils;
import org.lazywizard.lazylib.combat.DefenseUtils;
import org.lwjgl.util.vector.Vector;
import org.lwjgl.util.vector.Vector2f;

public class kiprad_impulseLanceOnHitEffect implements BeamEffectPlugin {

    private final float IMPULSE_SCALAR = -1/4000;
    private final float MAXIMUM_IMPULSE_SPEED = 2000f;

    private float impulseSpeed;
    private float distance;

    @Override
    public void advance (float amount, CombatEngineAPI engine, BeamAPI beam) {
        ShipAPI ship = (ShipAPI) beam.getSource();
        CombatEntityAPI target = beam.getDamageTarget();

        if(beam.getDamageTarget() instanceof ShipAPI || beam.getDamageTarget() instanceof Asteroid) {
            if (target != null && ship != null && beam.getBrightness() >= 1f) {
                distance = MathUtils.getDistance(target, ship);
                if (!VectorUtils.isZeroVector(beam.getTo())) {
                    ship.getVelocity().set(VectorUtils.getDirectionalVector(ship.getLocation(), beam.getTo()));
                }
            }

            impulseSpeed = (float) (IMPULSE_SCALAR * Math.pow((distance - beam.getWeapon().getRange()), 2f)) + MAXIMUM_IMPULSE_SPEED ; //"y=-\frac{1}{4000}\left(x\ -2000\right)^{2}\ +1200" -- copy this into desmos.com/calculator, shows the function that determines the lance boost

            if (!VectorUtils.isZeroVector(ship.getVelocity())) {
                ship.getVelocity().normalise();
                ship.getVelocity().scale(impulseSpeed);
            }
        }
    }
}
