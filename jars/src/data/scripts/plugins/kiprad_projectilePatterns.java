package data.scripts.plugins;

import com.fs.starfarer.api.Global;
import com.fs.starfarer.api.combat.*;
import com.fs.starfarer.api.input.InputEventAPI;

import java.util.ArrayList;
import java.util.List;

/* by tomatopaste
features code to script dual linked ballistic projectiles to travel in oppositely synced sine waves.
    the MOVE_SPEED should have the same value as projSpeed in the weapons.csv for range to work properly

        contents:
    1.  -Add a sinusoidal motion to the "kiprad_everest_shot" projectile
    2.  -Add a sinusoidal motion to the "kiprad_radeon_shot" projectile

*/


public class kiprad_projectilePatterns extends BaseEveryFrameCombatPlugin {

    private CombatEngineAPI engine;

    private final String EVEREST_SHOT_ID = "kiprad_everest_shot";
    private final String RADEON_SHOT_ID = "kiprad_radeon_shot";

    //programmer defined parameters
    private final float EVEREST_AMPLITUDE = 6f; //10f default
    private final float EVEREST_FREQUENCY = 60f; //60f default
    private float everestMoveSpeed = 800f;

    private final float RADEON_AMPLITUDE = 8f; //8f default
    private final float RADEON_FREQUENCY = 30f; //30f default
    private float radeonMoveSpeed = 2000f;

    //initialise variables used by the sine calc
    private float c;
    private float s;
    private float wobble;

    //two separate lists needed for the different behaviour
    //everest
    private List<DamagingProjectileAPI> targetEverestProjectileList1 = new ArrayList<>();
    private List<DamagingProjectileAPI> targetEverestProjectileList2 = new ArrayList<>();
    //radeon
    private List<DamagingProjectileAPI> targetRadeonProjectileList1 = new ArrayList<>();
    private List<DamagingProjectileAPI> targetRadeonProjectileList2 = new ArrayList<>();
    //it works ok  :P
    private boolean addToFirstList;

    @Override
    public void advance (float amount, List<InputEventAPI> events) {

        if (engine != Global.getCombatEngine()) {
            this.engine = Global.getCombatEngine();
        }

        if (engine.isPaused() || engine == null) {
            return;
        }

        List<DamagingProjectileAPI> projectileList = engine.getProjectiles();

        if (!projectileList.isEmpty()) {
            for (DamagingProjectileAPI item : projectileList) {

                //EVEREST PROJECTILE PATTERN

                if (item != null && item.getProjectileSpecId() != null && item.getProjectileSpecId().contentEquals(EVEREST_SHOT_ID) && engine.isEntityInPlay(item)) {

                    //if the everest shot is not in either projectile list, then it adds it to one of them based on whether the addToFirstList boolean is true, which should alternate with each new projectile
                    if (!targetEverestProjectileList1.contains(item) && !targetEverestProjectileList2.contains(item)) {
                        if (addToFirstList) {
                            targetEverestProjectileList2.add(item);
                            addToFirstList = false;
                        } else if (!targetEverestProjectileList2.contains(item)) {
                            targetEverestProjectileList1.add(item);
                            addToFirstList = true;
                        }
                    }

                    if (item.getElapsed() >= 0.1f) {
                        everestMoveSpeed = item.getWeapon().getProjectileSpeed();
                        if (targetEverestProjectileList1.contains(item) && item.getVelocity() != null) {

                            //more epic maths to calc the sine wave with negative amplitude
                            c = (float)(Math.cos(Math.toRadians(item.getFacing())));
                            s = (float)(Math.sin(Math.toRadians(item.getFacing())));

                            wobble = -1f * EVEREST_AMPLITUDE * (int) Math.round(Math.cos(EVEREST_FREQUENCY * item.getElapsed()) * EVEREST_FREQUENCY);

                            item.getVelocity().setX(c * everestMoveSpeed - s * wobble);
                            item.getVelocity().setY(s * everestMoveSpeed + c * wobble);
                        }

                        if (targetEverestProjectileList2.contains(item) && item.getVelocity() != null) {

                            //epic maths to calc the sine wav
                            c = (float)(Math.cos(Math.toRadians(item.getFacing())));
                            s = (float)(Math.sin(Math.toRadians(item.getFacing())));

                            wobble = EVEREST_AMPLITUDE * (int) Math.round(Math.cos(EVEREST_FREQUENCY * item.getElapsed()) * EVEREST_FREQUENCY);

                            item.getVelocity().setX(c * everestMoveSpeed - s * wobble);
                            item.getVelocity().setY(s * everestMoveSpeed + c * wobble);
                        }
                    }
                }

                //RADEON PROJECTILE PATTERN todo - figure out how to put this in one procedure

                if (item != null && item.getProjectileSpecId() != null && item.getProjectileSpecId().contentEquals(RADEON_SHOT_ID) && engine.isEntityInPlay(item)) {

                    //if the everest shot is not in either projectile list, then it adds it to one of them based on whether the addToFirstList boolean is true, which should alternate with each new projectile
                    if (!targetRadeonProjectileList1.contains(item) && !targetRadeonProjectileList2.contains(item)) {
                        if (addToFirstList) {
                            targetRadeonProjectileList1.add(item);
                            addToFirstList = false;
                        } else if (!targetRadeonProjectileList1.contains(item)) {
                            targetRadeonProjectileList2.add(item);
                            addToFirstList = true;
                        }
                    }

                    if (item.getElapsed() >= 0.1f) {
                        radeonMoveSpeed = item.getWeapon().getProjectileSpeed();
                        if (targetRadeonProjectileList1.contains(item) && item.getVelocity() != null) {

                            //more epic maths to calc the sine wave with negative amplitude but wanna hear a joke?
                            c = (float)(Math.cos(Math.toRadians(item.getFacing())));
                            s = (float)(Math.sin(Math.toRadians(item.getFacing())));

                            wobble = -1f * RADEON_AMPLITUDE * (int) Math.round(Math.cos(RADEON_FREQUENCY * item.getElapsed()) * RADEON_FREQUENCY);

                            item.getVelocity().setX(c * radeonMoveSpeed - s * wobble);
                            item.getVelocity().setY(s * radeonMoveSpeed + c * wobble);
                        }

                        if (targetRadeonProjectileList2.contains(item) && item.getVelocity() != null) {

                            //SiNe WaVe CaLc why did the pather cross the road?
                            c = (float)(Math.cos(Math.toRadians(item.getFacing())));
                            s = (float)(Math.sin(Math.toRadians(item.getFacing())));

                            wobble = RADEON_AMPLITUDE * (int) Math.round(Math.cos(RADEON_FREQUENCY * item.getElapsed()) * RADEON_FREQUENCY);

                            item.getVelocity().setX(c * radeonMoveSpeed - s * wobble);
                            item.getVelocity().setY(s * radeonMoveSpeed + c * wobble);
                            //to get to the other side amirite
                        }
                    }
                }
            }
        }
        //removes projectiles from the list once they aren't in play
        //everest lists
        List<DamagingProjectileAPI> projToRemove = new ArrayList<>();
        for (DamagingProjectileAPI test : targetEverestProjectileList1) {
            if (!engine.isEntityInPlay(test) && targetEverestProjectileList1 != null) {
                projToRemove.add(test);
            }
        }
        for (DamagingProjectileAPI test : targetEverestProjectileList2) {
            if (!engine.isEntityInPlay(test) && targetEverestProjectileList2 != null) {
                projToRemove.add(test);
            }
        }
        //radeon lists
        for (DamagingProjectileAPI test : targetRadeonProjectileList1) {
            if (!engine.isEntityInPlay(test) && targetRadeonProjectileList1 != null) {
                projToRemove.add(test);
            }
        }
        for (DamagingProjectileAPI test : targetRadeonProjectileList2) {
            if (!engine.isEntityInPlay(test) && targetRadeonProjectileList2 != null) {
                projToRemove.add(test);
            }
        }

        //removes projectiles from the garbage list
        for (DamagingProjectileAPI rem : projToRemove) {
            if(targetEverestProjectileList1 != null) {
                targetEverestProjectileList1.remove(rem);
            }
            if(targetEverestProjectileList2 != null) {
                targetEverestProjectileList2.remove(rem);
            }
            if(targetRadeonProjectileList1 != null) {
                targetRadeonProjectileList1.remove(rem);
            }
            if(targetRadeonProjectileList2 != null) {
                targetRadeonProjectileList2.remove(rem);
            }
        }

        /*////////
        //MISSILES
        ////////*/

        //nothing here yet
    }
}
