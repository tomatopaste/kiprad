package data.scripts.plugins;

import com.fs.starfarer.api.Global;
import com.fs.starfarer.api.combat.*;
import com.fs.starfarer.api.input.InputEventAPI;
import org.lazywizard.lazylib.MathUtils;
import org.lazywizard.lazylib.VectorUtils;
import org.lwjgl.util.vector.Vector2f;

import java.util.List;

public class kiprad_droneCoronaPlugin extends BaseEveryFrameCombatPlugin {
    private CombatEngineAPI engine;

    private String SYSTEM_ID = "kiprad_droneCorona";
    private String ACTIVE_WEAPON_ID = "hil";
    private String DEFAULT_WEAPON_ID = "pdburst";

    @Override
    public void advance (float amount, List<InputEventAPI> events) {

        if (engine != Global.getCombatEngine()) {
            this.engine = Global.getCombatEngine();
        }

        if (engine.isPaused() || engine == null) {
            return;
        }

        List<ShipAPI> shipList = engine.getShips();

        for (ShipAPI ship : shipList) {
            ShipSystemAPI system = ship.getSystem();

            if (system != null && system.getId().equals(SYSTEM_ID) && ship.isAlive()) {
                DroneLauncherShipSystemAPI droneSystem = (DroneLauncherShipSystemAPI) system;
                List<ShipAPI> droneList = ship.getDeployedDrones();
                boolean isInFocusMode = !ship.isPullBackFighters();

                if (ship == engine.getPlayerShip()) {
                    if (isInFocusMode) {
                        engine.maintainStatusForPlayerShip("KIPRAD_STATUS_KEY_1", "graphics/icons/hullsys/drone_pd_high.png", "Focus Mode Active", "Bottom Text", true);
                    } else {
                        engine.maintainStatusForPlayerShip("KIPRAD_STATUS_KEY_2", "graphics/icons/hullsys/drone_pd_high.png", "Focus Mode Inactive", "Bottom Text", false);
                    }
                }

                if (droneList != null) {
                    for (ShipAPI drone : droneList) {
                        int droneIndex = droneSystem.getIndex(drone);

                        float angle;
                        float targetAngle = 0f;
                        if (isInFocusMode) {
                            switch (droneIndex) {
                                default:
                                case 0:
                                    targetAngle = 0f;
                                    break;
                                case 1:
                                    targetAngle = 5f;
                                    break;
                                case 2:
                                    targetAngle = -5f;
                                    break;
                            }
                        } /*else {
                            switch (droneIndex) {
                                default:
                                case 0:
                                    targetAngle = 0f;
                                    break;
                                case 1:
                                    targetAngle = 50f;
                                    break;
                                case 2:
                                    targetAngle = -50f;
                                    break;
                            }
                        }*/
                        targetAngle += ship.getFacing(); //rotates the target angle to account for ship facing angle

                        Vector2f droneDirectionFromShipVector = VectorUtils.getDirectionalVector(ship.getLocation(), drone.getLocation());
                        float droneAngle = VectorUtils.getFacing(droneDirectionFromShipVector);

                        float angleDistance = (targetAngle - droneAngle) * 0.75f;

                        angle = targetAngle;

                        Vector2f droneTargetPosition = MathUtils.getPointOnCircumference(ship.getLocation(), ship.getShieldRadiusEvenIfNoShield(), angle);     //the drone should be protected by shields

                        if (isInFocusMode) {
                            snapTo(ship, 0.5f, drone, droneTargetPosition);
                        }
                        boolean isShipSystemActive = system.getAmmo() - droneList.size() < system.getMaxAmmo();

                        if (!isShipSystemActive) {
                            List<WeaponAPI> droneWeapons = drone.getUsableWeapons();

                            /*
                            switch (droneIndex) {
                                default:
                                case 0:
                                    if (drone.getSystem().isOn()) {
                                        drone.useSystem();
                                    }
                                case 1:
                                    for (WeaponAPI weapon : droneWeapons) {
                                        if (weapon.getId().equals(DEFAULT_WEAPON_ID)) {
                                            weapon.repair();
                                        }
                                    }
                                case 2:
                                    for (WeaponAPI weapon : droneWeapons) {
                                        if (weapon.getId().equals(DEFAULT_WEAPON_ID)) {
                                            weapon.repair();
                                        }
                                    }
                            } */
                        } else {     //what happens if drones are out / system on
                            List<WeaponAPI> droneWeapons = drone.getUsableWeapons();

                            /*
                            switch (droneIndex) {
                                default:
                                case 0:
                                    if (!drone.getSystem().isOn()) {
                                        drone.useSystem();
                                    }
                                case 1:
                                    for (WeaponAPI weapon : droneWeapons) {
                                        weapon.disable(true);
                                    }
                                case 2:
                                    for (WeaponAPI weapon : droneWeapons) {
                                        weapon.disable(true);
                                    }
                            } */
                        }
                    }
                }
            }
        }
    }

    //!!from here downwards is methods liberated with permission from Lloyd's snsp_center_escort script, IF YOU READ THIS AND KNOW HOW TO OVERRIDE AI FOR SMOOTH DRONE MOVEMENT THEN I SALUTE YOU
    private void snapTo(
            ShipAPI target,
            float magnitude,
            ShipAPI drone,
            Vector2f targetPosition
    ) {
        if (null == target) {
            return;
        }

        magnitude = MathUtils.clamp(magnitude, 0F, 1F);

        Vector2f locationDelta = Vector2f.sub(targetPosition, drone.getLocation(), new Vector2f());
        Vector2f velocityDelta = Vector2f.sub(target.getVelocity(), drone.getVelocity(), new Vector2f());
        float facingDelta = target.getFacing() - drone.getFacing();
        float angularVelocityDelta = target.getAngularVelocity() - drone.getAngularVelocity();

        drone.getLocation().x += locationDelta.x * magnitude;
        drone.getLocation().y += locationDelta.y * magnitude;
        drone.getVelocity().x += velocityDelta.x * magnitude;
        drone.getVelocity().y += velocityDelta.y * magnitude;
        drone.setFacing(drone.getFacing() + facingDelta * magnitude);
        drone.setAngularVelocity(drone.getAngularVelocity() + angularVelocityDelta * magnitude);
    }
}
