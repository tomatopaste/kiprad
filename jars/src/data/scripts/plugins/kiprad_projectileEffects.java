package data.scripts.plugins;

import com.fs.starfarer.api.Global;
import com.fs.starfarer.api.combat.*;
import com.fs.starfarer.api.input.InputEventAPI;

import java.awt.*;
import java.util.ArrayList;
import java.util.List;

/* by tomatopaste
    contents:
    1.  -Add a pink flicker effect to missiles fired from ship with RIM hullmod
*/


public class kiprad_projectileEffects extends BaseEveryFrameCombatPlugin {

    private CombatEngineAPI engine;

    public final String REPURPOSED_IMPELLER_MATRIX_ID = "kiprad_repurposedImpellerMatrix";
    public final Color RIM_MISSILE_JITTER_COLOUR = new Color(250, 20, 250);

    @Override
    public void advance (float amount, List<InputEventAPI> events) {

        if (engine != Global.getCombatEngine()) {
            this.engine = Global.getCombatEngine();
        }

        if (engine.isPaused() || engine == null) {
            return;
        }

        /*////////
        //MISSILES
        ////////*/

        List<MissileAPI> missileList = engine.getMissiles();

        if (!missileList.isEmpty()) {
            for (MissileAPI item : missileList) {
                if (item != null && item.getSource().getVariant() != null) {
                    //add jitter to missiles from ship with this hullmod
                    if (item.getSource().getVariant().getHullMods().contains(REPURPOSED_IMPELLER_MATRIX_ID)) {
                        item.setJitter(item, RIM_MISSILE_JITTER_COLOUR, 2f, 4, 0f, 5f);
                    }
                }
            }
        }
    }
}
