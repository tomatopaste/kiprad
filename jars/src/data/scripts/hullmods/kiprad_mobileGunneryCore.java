package data.scripts.hullmods;

import com.fs.starfarer.api.combat.BaseHullMod;
import com.fs.starfarer.api.combat.MutableShipStatsAPI;
import com.fs.starfarer.api.combat.ShipAPI;
import com.fs.starfarer.api.combat.ShipAPI.*;

public class kiprad_mobileGunneryCore extends BaseHullMod {

    public static final float WEAPON_RANGE_BONUS_PERCENT = 35f;

    @Override
    public void applyEffectsToFighterSpawnedByShip(ShipAPI fighter, ShipAPI ship, String id) {
        fighter.getMutableStats().getEnergyWeaponRangeBonus().modifyPercent(id, WEAPON_RANGE_BONUS_PERCENT);
        fighter.getMutableStats().getBallisticWeaponRangeBonus().modifyPercent(id, WEAPON_RANGE_BONUS_PERCENT);
    }


    @Override
    public String getDescriptionParam(int index, HullSize hullSize) {
        if (index == 0) {
            return "" + (int) WEAPON_RANGE_BONUS_PERCENT + "%";
        }
        return null;
    }
}
