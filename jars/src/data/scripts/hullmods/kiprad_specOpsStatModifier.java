package data.scripts.hullmods;

import com.fs.starfarer.api.combat.BaseHullMod;
import com.fs.starfarer.api.combat.MutableShipStatsAPI;
import com.fs.starfarer.api.combat.ShipAPI;
import com.fs.starfarer.api.combat.ShipAPI.HullSize;
import com.fs.starfarer.api.util.Misc;

import java.util.HashSet;
import java.util.Set;

public class kiprad_specOpsStatModifier extends BaseHullMod {

    private final float SENSOR_PROFILE_MOD_PERCENT = -80f;
    private final float VENT_RATE_MOD_PERCENT = 50f;
    private final float ZERO_FLUX_BOOST_MOD_FLAT = 10f;

    private static final Set<String> BLOCKED_HULLMODS = new HashSet<>(1);

    static {
        BLOCKED_HULLMODS.add("safetyoverrides");
    }

    @Override
    public void applyEffectsBeforeShipCreation(HullSize hullSize, MutableShipStatsAPI stats, String id) {
        stats.getSensorProfile().modifyPercent(id, SENSOR_PROFILE_MOD_PERCENT);
        stats.getVentRateMult().modifyPercent(id, VENT_RATE_MOD_PERCENT);
        stats.getZeroFluxSpeedBoost().modifyFlat(id, ZERO_FLUX_BOOST_MOD_FLAT);
    }

    @Override
    public void applyEffectsAfterShipCreation (ShipAPI ship, String id) {
        for (String item : BLOCKED_HULLMODS) {
            if (ship.getVariant().getHullMods().contains(item)) {
                ship.getVariant().removeMod(item);
            }
        }
    }

    @Override
    public String getDescriptionParam(int index, HullSize hullSize) {
        if (index == 0) {
            return "" + (int) SENSOR_PROFILE_MOD_PERCENT + "%";
        }
        if (index == 1) {
            return "" + (int) VENT_RATE_MOD_PERCENT + "%";
        }
        if (index == 2) {
            return "" + (int) ZERO_FLUX_BOOST_MOD_FLAT;
        }
        if (index == 3) {
            return "" + "Precludes installation of Safety Overrides";
        }
        return null;
    }

    @Override
    public boolean isApplicableToShip(ShipAPI ship) {
        return (!ship.getVariant().getHullMods().contains("safetyoverrides"));
    }

    @Override
    public String getUnapplicableReason(ShipAPI ship) {
        if (ship.getVariant().getHullMods().contains("safetyoverrides")) {
            return "Incompatible with Safety Overrides";
        }
        return null;
    }
}
