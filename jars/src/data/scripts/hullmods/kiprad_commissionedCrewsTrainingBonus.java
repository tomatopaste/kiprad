package data.scripts.hullmods;

import com.fs.starfarer.api.combat.BaseHullMod;
import com.fs.starfarer.api.combat.MutableShipStatsAPI;
import com.fs.starfarer.api.combat.ShipAPI;
import com.fs.starfarer.api.combat.ShipAPI.HullSize;

public class kiprad_commissionedCrewsTrainingBonus extends BaseHullMod {

    public static final float HARD_FLUX_DISSIPATION_FLAT_MOD = 0.05f;

    @Override
    public void applyEffectsBeforeShipCreation(HullSize hullSize, MutableShipStatsAPI stats, String id) {
        stats.getHardFluxDissipationFraction().modifyFlat(id, HARD_FLUX_DISSIPATION_FLAT_MOD);
    }

    @Override //All you need is this to be honest. The framework will do everything on its own.
    public void applyEffectsAfterShipCreation (ShipAPI ship, String id) {
        if (ship.getVariant().hasHullMod("CHM_commission")) {
            ship.getVariant().removeMod("CHM_commission");
        }
        // This is to remove the unnecessary dummy hull mod. Unless the player want it... but nah!
    }

    @Override
    public String getDescriptionParam(int index, HullSize hullSize) {
        if (index == 0) {
            return "" + (int) HARD_FLUX_DISSIPATION_FLAT_MOD * 100 + "%";
        }
        return null;
    }
}
