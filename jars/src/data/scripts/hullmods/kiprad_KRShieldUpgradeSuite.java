package data.scripts.hullmods;

import com.fs.starfarer.api.combat.BaseHullMod;
import com.fs.starfarer.api.combat.ShipAPI;
import com.fs.starfarer.campaign.fleet.SmoothMovementModule;
import com.fs.starfarer.combat.entities.Ship;

public class kiprad_KRShieldUpgradeSuite extends BaseHullMod {

    public static final float SHIELD_EFFICIENCY_PERCENT_DECREASE = -15f;
    public static final float SHIELD_UPKEEP_PERCENT_DECREASE = -25f;
    public static final float SHIELD_UNFOLD_SPEED_PERCENT_BONUS = 50f;
    public static final float SHIELD_MOVEMENT_SPEED_PERCENT_BONUS = 100f;

    @Override
    public void applyEffectsToFighterSpawnedByShip(ShipAPI fighter, ShipAPI ship, String id) {
        fighter.getMutableStats().getShieldDamageTakenMult().modifyPercent(id, SHIELD_EFFICIENCY_PERCENT_DECREASE);
        fighter.getMutableStats().getShieldUpkeepMult().modifyPercent(id, SHIELD_UPKEEP_PERCENT_DECREASE);
        fighter.getMutableStats().getShieldUnfoldRateMult().modifyPercent(id, SHIELD_UNFOLD_SPEED_PERCENT_BONUS);
        fighter.getMutableStats().getShieldTurnRateMult().modifyPercent(id, SHIELD_MOVEMENT_SPEED_PERCENT_BONUS);
    }

    @Override
    public String getDescriptionParam(int index, ShipAPI.HullSize hullSize) {
        if (index == 0) {
            return "" + (int) SHIELD_EFFICIENCY_PERCENT_DECREASE + "%";
        }
        if (index == 1) {
            return "" + (int) SHIELD_UPKEEP_PERCENT_DECREASE + "%";
        }
        if (index == 2) {
            return "" + (int) SHIELD_UNFOLD_SPEED_PERCENT_BONUS + "%";
        }
        if (index == 3) {
            return "" + (int) SHIELD_MOVEMENT_SPEED_PERCENT_BONUS + "%";
        }
        return null;
    }

    @Override
    public boolean isApplicableToShip(ShipAPI ship) {
        return (ship.getHullSpec().getHullId().startsWith("kiprad_"));
    }

    @Override
    public String getUnapplicableReason(ShipAPI ship) {
        if (!ship.getHullSpec().getHullId().startsWith("kiprad_")) {
            return "Only installable on Kipling Radiative ships";
        }
        return null;
    }
}
