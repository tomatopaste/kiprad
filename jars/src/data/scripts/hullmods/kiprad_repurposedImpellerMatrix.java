package data.scripts.hullmods;

import com.fs.starfarer.api.combat.BaseHullMod;
import com.fs.starfarer.api.combat.MutableShipStatsAPI;
import com.fs.starfarer.api.combat.ShipAPI;
import com.fs.starfarer.api.combat.ShipAPI.*;
import com.fs.starfarer.combat.H;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public class kiprad_repurposedImpellerMatrix extends BaseHullMod {

    public static final Map<HullSize, Float> MAX_SPEED_PER_HULL_SIZE_PERCENT = new HashMap<>(4);
    private static final Set<String> BLOCKED_HULLMODS = new HashSet<>(3);

    static {
        MAX_SPEED_PER_HULL_SIZE_PERCENT.put(HullSize.FRIGATE, -35f);
        MAX_SPEED_PER_HULL_SIZE_PERCENT.put(HullSize.DESTROYER, -30f);
        MAX_SPEED_PER_HULL_SIZE_PERCENT.put(HullSize.CRUISER, -25f);
        MAX_SPEED_PER_HULL_SIZE_PERCENT.put(HullSize.CAPITAL_SHIP, -20f);
    }

    static {
        BLOCKED_HULLMODS.add("unstable_injector");
        BLOCKED_HULLMODS.add("safetyoverrides");
        BLOCKED_HULLMODS.add("eccm");
    }

    public static final float MISSILE_TRACKING_BONUS_PERCENT = 150f;
    public static final float MISSILE_LOADING_SPEED_BONUS_PERCENT = 25f;
    public static final float MISSILE_SPEED_BONUS_PERCENT = 25f;
    public static final float MISSILE_RANGE_BONUS_PERCENT = 10f;

    @Override
    public void applyEffectsAfterShipCreation(ShipAPI ship, String id) {
        for (String item : BLOCKED_HULLMODS) {
            if (ship.getVariant().getHullMods().contains(item)) {
                ship.getVariant().removeMod(item);
            }
        }
    }

    @Override
    public void applyEffectsBeforeShipCreation(HullSize hullSize, MutableShipStatsAPI stats, String id) {
        stats.getMissileGuidance().modifyPercent(id, MISSILE_TRACKING_BONUS_PERCENT);
        stats.getMissileRoFMult().modifyPercent(id, MISSILE_LOADING_SPEED_BONUS_PERCENT);
        stats.getMissileMaxSpeedBonus().modifyPercent(id, MISSILE_SPEED_BONUS_PERCENT);
        stats.getMissileAccelerationBonus().modifyPercent(id, MISSILE_SPEED_BONUS_PERCENT);
        stats.getMissileMaxTurnRateBonus().modifyPercent(id, MISSILE_SPEED_BONUS_PERCENT);
        stats.getMissileWeaponRangeBonus().modifyPercent(id, MISSILE_RANGE_BONUS_PERCENT);

        float mod = MAX_SPEED_PER_HULL_SIZE_PERCENT.get(hullSize);
        stats.getMaxSpeed().modifyPercent(id, mod);
        stats.getZeroFluxSpeedBoost().modifyPercent(id, mod);
    }

    @Override
    public String getDescriptionParam(int index, HullSize hullSize) {
        if (index == 0) {
            return "" + (int) MISSILE_SPEED_BONUS_PERCENT + "%";
        }
        if (index == 1) {
            return "" + (int) MISSILE_RANGE_BONUS_PERCENT + "%";
        }
        if (index == 2) {
            return "" + (int) MISSILE_LOADING_SPEED_BONUS_PERCENT + "%";
        }
        if (index == 3) {
            return "" + MAX_SPEED_PER_HULL_SIZE_PERCENT.get(HullSize.FRIGATE).intValue() + "%";
        }
        if (index == 4) {
            return "" + MAX_SPEED_PER_HULL_SIZE_PERCENT.get(HullSize.DESTROYER).intValue() + "%";
        }
        if (index == 5) {
            return  "" + MAX_SPEED_PER_HULL_SIZE_PERCENT.get(HullSize.CRUISER).intValue() + "%";
        }
        if (index == 6) {
            return "" + MAX_SPEED_PER_HULL_SIZE_PERCENT.get(HullSize.CAPITAL_SHIP).intValue() + "%";
        }
        if (index == 7) {
            return "" + "Precludes installation of Safety Overrides, ECCM Package and Unstable Injector";
        }
        return null;
    }

    @Override
    public boolean isApplicableToShip(ShipAPI ship) {
        return (!ship.getVariant().getHullMods().contains("unstable_injector") &&
                !ship.getVariant().getHullMods().contains("safetyoverrides") &&
                !ship.getVariant().getHullMods().contains("eccm")
        );
    }

    @Override
    public String getUnapplicableReason(ShipAPI ship) {
        if (ship.getVariant().getHullMods().contains("unstable_injector")) {
            return "Incompatible with Unstable Injector";
        }

        if (ship.getVariant().getHullMods().contains("safetyoverrides")) {
            return "Incompatible with Safety Overrides";
        }

        if (ship.getVariant().getHullMods().contains("eccm")) {
            return "Incompatible with ECCM Package";
        }

        return null;
    }
}
