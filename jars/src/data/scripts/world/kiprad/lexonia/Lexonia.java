package data.scripts.world.kiprad.lexonia;

import com.fs.starfarer.api.Global;
import com.fs.starfarer.api.InteractionDialogImageVisual;
import com.fs.starfarer.api.campaign.*;
import com.fs.starfarer.api.campaign.econ.MarketAPI;
import com.fs.starfarer.api.fleet.FleetMemberAPI;
import com.fs.starfarer.api.fleet.FleetMemberType;
import com.fs.starfarer.api.impl.campaign.ids.*;
import com.fs.starfarer.api.impl.campaign.procgen.NebulaEditor;
import com.fs.starfarer.api.impl.campaign.procgen.StarAge;
import com.fs.starfarer.api.impl.campaign.procgen.StarSystemGenerator;
import com.fs.starfarer.api.impl.campaign.rulecmd.salvage.MarketCMD;
import com.fs.starfarer.api.impl.campaign.terrain.HyperspaceTerrainPlugin;
import com.fs.starfarer.api.util.Misc;
import data.scripts.world.kiprAddMarketplace;
import sound.I;

import java.awt.Color;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Lexonia implements SectorGeneratorPlugin{

    public void generate(SectorAPI sector) {

        //initialise system
        StarSystemAPI lexoniaSystem = sector.createStarSystem("Lexonia");
        lexoniaSystem.getLocation().set(-21000, 1000);
        lexoniaSystem.setLightColor(new Color(255, 235, 255));
        lexoniaSystem.setBackgroundTextureFilename("graphics/backgrounds/background2.jpg");

        //set up star
        PlanetAPI lexoniaStar = lexoniaSystem.initStar("lexoniaStar", "star_orange", 900, -21000, 1000, 250);
        lexoniaStar.setCustomDescriptionId("kiprad_lexonia");

        //generate up to three entities in the centre of the system and returns the orbit radius of the furthest entity
        float outermostOrbitDistance = StarSystemGenerator.addOrbitingEntities(lexoniaSystem, lexoniaStar, StarAge.YOUNG, 1, 3, 4000, 1, false);

        ///planets///

        PlanetAPI lexoniaStilleta = lexoniaSystem.addPlanet("kiprad_stilleta", lexoniaStar, "Stilleta", "ice_giant", 180, 400, outermostOrbitDistance + 7000, 625);
        lexoniaStilleta.setCustomDescriptionId("kiprad_stiletta");
        lexoniaStilleta.getSpec().setGlowColor(new Color(255, 200, 255, 255));
        lexoniaStilleta.getSpec().setUseReverseLightForGlow(true);
        lexoniaStilleta.setInteractionImage("illustrations", "kiprad_freighters");
        lexoniaStilleta.applySpecChanges();

        PlanetAPI lexoniaCollia = lexoniaSystem.addPlanet("kiprad_collia", lexoniaStilleta, "Collia", "toxic", 50, 100, 1400, 60);
        lexoniaCollia.setCustomDescriptionId("kiprad_collia");
        lexoniaCollia.getSpec().setGlowColor(new Color(200, 255, 200, 255));
        lexoniaCollia.getSpec().setUseReverseLightForGlow(true);
        lexoniaCollia.setInteractionImage("illustrations", "kiprad_desert_moons_ruins");
        lexoniaCollia.applySpecChanges();

        PlanetAPI lexoniaDell = lexoniaSystem.addPlanet("kiprad_dell", lexoniaStar, "Dell", "jungle", 300, 140, outermostOrbitDistance + 1500, 215);
        lexoniaDell.setCustomDescriptionId("kiprad_dell");
        lexoniaDell.getSpec().setGlowColor(new Color(255, 255, 255, 255));
        lexoniaDell.getSpec().setUseReverseLightForGlow(true);
        lexoniaDell.setInteractionImage("illustrations", "kiprad_urban02");
        lexoniaDell.applySpecChanges();

        PlanetAPI lexoniaAtrasti = lexoniaSystem.addPlanet("kiprad_atrasti", lexoniaStar, "Atrasti", "barren", 300, 110, outermostOrbitDistance + 16000, 800);
        lexoniaAtrasti.setCustomDescriptionId("kiprad_atrasti");
        lexoniaAtrasti.getSpec().setGlowColor(new Color(255, 255, 255, 255));
        lexoniaAtrasti.getSpec().setUseReverseLightForGlow(true);
        lexoniaAtrasti.setInteractionImage("illustrations", "kiprad_vacuum_colony");
        lexoniaAtrasti.applySpecChanges();

        //market attribute assignment
        MarketAPI colliaMarketplace = kiprAddMarketplace.addMarketplace(
                "kiprad",
                lexoniaCollia,
                null,
                "Collia Military Division",
                5,
                new ArrayList<>(Arrays.asList(
                        Conditions.RUINS_SCATTERED,
                        Conditions.VOLATILES_PLENTIFUL,
                        Conditions.TOXIC_ATMOSPHERE,
                        Conditions.TECTONIC_ACTIVITY,
                        Conditions.ORE_ABUNDANT,
                        Conditions.LOW_GRAVITY,
                        Conditions.POPULATION_5)),
                new ArrayList<>(Arrays.asList(
                        Submarkets.SUBMARKET_STORAGE,
                        Submarkets.SUBMARKET_BLACK,
                        Submarkets.SUBMARKET_OPEN,
                        Submarkets.GENERIC_MILITARY)),
                new ArrayList<>(Arrays.asList(
                        Industries.POPULATION,
                        Industries.SPACEPORT,
                        Industries.HEAVYBATTERIES,
                        Industries.HIGHCOMMAND,
                        Industries.FUELPROD,
                        Industries.MINING,
                        Industries.BATTLESTATION_HIGH
                )),
                true,
                false);

        MarketAPI dellMarketplace = kiprAddMarketplace.addMarketplace(
                "kiprad",
                lexoniaDell,
                null,
                "Dell Commercialis",
                7,
                new ArrayList<>(Arrays.asList(
                        Conditions.RUINS_WIDESPREAD,
                        Conditions.ORGANICS_COMMON,
                        Conditions.FARMLAND_ADEQUATE,
                        Conditions.INIMICAL_BIOSPHERE,
                        Conditions.JUNGLE,
                        Conditions.EXTREME_WEATHER,
                        Conditions.TECTONIC_ACTIVITY,
                        Conditions.ORE_MODERATE,
                        Conditions.RARE_ORE_SPARSE,
                        Conditions.HABITABLE,
                        Conditions.POPULATION_7)),
                new ArrayList<>(Arrays.asList(
                        Submarkets.SUBMARKET_STORAGE,
                        Submarkets.SUBMARKET_BLACK,
                        Submarkets.SUBMARKET_OPEN)),
                new ArrayList<>(Arrays.asList(
                        Industries.POPULATION,
                        Industries.MEGAPORT,
                        Industries.HEAVYBATTERIES,
                        Industries.PATROLHQ,
                        Industries.WAYSTATION,
                        Industries.STARFORTRESS_MID,
                        Industries.MINING,
                        Industries.REFINING,
                        Industries.FARMING
                )),
                true,
                false);
        dellMarketplace.addIndustry(Industries.ORBITALWORKS, new ArrayList<>(Arrays.asList(Items.PRISTINE_NANOFORGE)));

        MarketAPI atrastiMarketplace = kiprAddMarketplace.addMarketplace(
                "pirates",
                lexoniaAtrasti,
                null,
                "Atrasti",
                4,
                new ArrayList<>(Arrays.asList(
                        Conditions.RUINS_SCATTERED,
                        Conditions.VOLATILES_DIFFUSE,
                        Conditions.NO_ATMOSPHERE,
                        Conditions.VERY_COLD,
                        Conditions.ORE_SPARSE,
                        Conditions.POPULATION_4)),
                new ArrayList<>(Arrays.asList(
                        Submarkets.SUBMARKET_STORAGE,
                        Submarkets.SUBMARKET_BLACK,
                        Submarkets.SUBMARKET_OPEN)),
                new ArrayList<>(Arrays.asList(
                        Industries.POPULATION,
                        Industries.SPACEPORT,
                        Industries.GROUNDDEFENSES,
                        Industries.PATROLHQ,
                        Industries.MINING
                )),
                false,
                false);


        //nebula
        SectorEntityToken lexoniaNebula = Misc.addNebulaFromPNG("data/campaign/terrain/eos_nebula.png", 0, 0, lexoniaSystem, "terrain", "nebula_blue", 4, 4, StarAge.AVERAGE);

        //asteroids and dust
        lexoniaSystem.addAsteroidBelt(lexoniaStar, 80, outermostOrbitDistance + 500, 255, 190, 220, Terrain.ASTEROID_BELT, "The Stones");
        lexoniaSystem.addRingBand(lexoniaStar, "misc", "rings_asteroids0", 256f, 2, Color.white, 256f, outermostOrbitDistance + 500, 70f, null, null);

        lexoniaSystem.addAsteroidBelt(lexoniaStar, 250, outermostOrbitDistance + 2000, 256, 400, 800, Terrain.ASTEROID_BELT, "Dellian Rim");
        lexoniaSystem.addRingBand(lexoniaStar, "misc", "rings_dust0", 512f, 2, Color.white, 512f, outermostOrbitDistance + 2000, 600, null, null);
        lexoniaSystem.addRingBand(lexoniaStar, "misc", "rings_asteroids0", 256f, 2, Color.white, 256f, outermostOrbitDistance + 2000, 1000, null, null);


        lexoniaSystem.addRingBand(lexoniaStar, "misc", "rings_dust0", 128f, 3, Color.white, 128, 1900, 750, Terrain.RING, "Outer Spans");

        //lexoniaSystem.addAsteroidBelt(lexoniaStar, 800, outermostOrbitDistance + 10000, 512, 121, 231, Terrain.ASTEROID_BELT, "The Stones");

        lexoniaSystem.addRingBand(lexoniaStar, "misc", "rings_dust0", 256f, 3, Color.white, 256, 16000, 850, Terrain.RING, "Coral Cloves");
        lexoniaSystem.addRingBand(lexoniaStar, "misc", "rings_ice0", 512f, 2, Color.white, 512f, 16000, 900f, null, null);


        ///custom entities///

        //makeshift comm relay
        SectorEntityToken lexoniaMakeshiftRelay = lexoniaSystem.addCustomEntity("kiprad_lexonia_relay", "Lexonia Relay", Entities.COMM_RELAY_MAKESHIFT, "kiprad");
        lexoniaMakeshiftRelay.setCircularOrbit(lexoniaStar, 270, outermostOrbitDistance + 4000, 950);

        // Inner system nav buoy - is legit
        SectorEntityToken lexoniaInnerNavBuoy = lexoniaSystem.addCustomEntity("kiprad_lexonia_inner_nav_buoy", "Collia Nav Buoy", Entities.NAV_BUOY, "kiprad");
        lexoniaInnerNavBuoy.setCircularOrbitPointingDown(lexoniaStar, 90, outermostOrbitDistance + 6000, 160);

        // Outer system nav buoy - ideally for the piraticals, but gets claimed by the kippers anyway
        SectorEntityToken lexoniaOuterNavBuoy = lexoniaSystem.addCustomEntity("kiprad_lexonia_outer_nav_buoy", "Auxiliary Nav Buoy", Entities.NAV_BUOY_MAKESHIFT, Factions.PIRATES);
        lexoniaOuterNavBuoy.setCircularOrbitPointingDown(lexoniaAtrasti, 90, 4000, 280);

        //legit sensor array
        SectorEntityToken lexoniaSensorArray = lexoniaSystem.addCustomEntity("kiprad_lexonia_sensor_array", "Collia Deep Scanning Platform", Entities.SENSOR_ARRAY, "kiprad");
        lexoniaSensorArray.setCircularOrbit(lexoniaStilleta, 180, 3000, 150);

        //Abandoned, mysterious station - oooooo spooky
        SectorEntityToken lexoniaStationAlpha = lexoniaSystem.addCustomEntity("kiprad_lexonia_station_alpha", "Lexonia Station Alpha", "kiprad_abandoned_station_small", "neutral");
        lexoniaStationAlpha.setCircularOrbitWithSpin(lexoniaStar, 110, outermostOrbitDistance + 8000, 240, 2, 8);
        lexoniaStationAlpha.setDiscoverable(true);
        lexoniaStationAlpha.setDiscoveryXP(1200f);
        lexoniaStationAlpha.setSensorProfile(0.35f);
        lexoniaStationAlpha.setCustomDescriptionId("kiprad_lexonia_station_alpha");
        lexoniaStationAlpha.setInteractionImage("illustrations", "kiprad_abandoned_station");
        lexoniaStationAlpha.getMemoryWithoutUpdate().set("$abandonedStation", true);
        Misc.setAbandonedStationMarket("kiprad_lexonia_station_alpha", lexoniaStationAlpha);
        //add some free cargo
        lexoniaStationAlpha.getMarket().getSubmarket(Submarkets.SUBMARKET_STORAGE).getCargo().addCommodity(Commodities.BETA_CORE, 1f);
        //add some mothballed ships to the list
        lexoniaStationAlpha.getMarket().getSubmarket(Submarkets.SUBMARKET_STORAGE).getCargo().addMothballedShip(FleetMemberType.SHIP, "kiprad_fremantle_Strike", "KRS Your Other Eye, Nelson");
        List<FleetMemberAPI> lexoniaStationAlphaMothballedShips = lexoniaStationAlpha.getMarket().getSubmarket(Submarkets.SUBMARKET_STORAGE).getCargo().getMothballedShips().getMembersListCopy();
        //todo - add some dmods
        //for (FleetMemberAPI ship : lexoniaStationAlphaMothballedShips) {
        //    ship.getHullSpec().addBuiltInMod(HullMods.DEFECTIVE_MANUFACTORY);
        //}

        //Abandoned, mysterious station - oooooo spooky
        SectorEntityToken lexoniaStationGamma = lexoniaSystem.addCustomEntity("kiprad_lexonia_station_gamma", "Lexonia Station Gamma", "kiprad_abandoned_station_small", "neutral");
        lexoniaStationGamma.setCircularOrbitWithSpin(lexoniaStar, 310, outermostOrbitDistance - 1000, 240, 2, 8);
        lexoniaStationGamma.setDiscoverable(true);
        lexoniaStationGamma.setDiscoveryXP(1200f);
        lexoniaStationGamma.setSensorProfile(0.35f);
        lexoniaStationGamma.setCustomDescriptionId("kiprad_lexonia_station_gamma");
        lexoniaStationGamma.setInteractionImage("illustrations", "kiprad_abandoned_station");
        lexoniaStationGamma.getMemoryWithoutUpdate().set("$abandonedStation", true);
        Misc.setAbandonedStationMarket("kiprad_lexonia_station_gamma", lexoniaStationGamma);
        //add some free cargo
        lexoniaStationGamma.getMarket().getSubmarket(Submarkets.SUBMARKET_STORAGE).getCargo().addCommodity(Commodities.HEAVY_MACHINERY, 35f);
        lexoniaStationGamma.getMarket().getSubmarket(Submarkets.SUBMARKET_STORAGE).getCargo().addCommodity(Commodities.LOBSTER, 12f);
        //add some mothballed ships to the list
        lexoniaStationGamma.getMarket().getSubmarket(Submarkets.SUBMARKET_STORAGE).getCargo().addMothballedShip(FleetMemberType.SHIP, "kiprad_mosman_Assault", "KRS The Spice is Right");
        List<FleetMemberAPI> lexoniaStationGammaMothballedShips = lexoniaStationAlpha.getMarket().getSubmarket(Submarkets.SUBMARKET_STORAGE).getCargo().getMothballedShips().getMembersListCopy();
        //todo - add some dmods

        /*
        //Less abandoned, less mysterious station - not spooky
        SectorEntityToken lexoniaStationBeta = lexoniaSystem.addCustomEntity("kiprad_lexonia_station_alpha", "Lexonia Station Alpha", "kiprad_abandoned_station_small", "neutral");
        lexoniaStationBeta.setCircularOrbitWithSpin(lexoniaStar, 110, outermostOrbitDistance + 8500, 300, 4, 15);
        lexoniaStationBeta.setDiscoverable(true);
        lexoniaStationBeta.setDiscoveryXP(1200f);
        lexoniaStationBeta.setSensorProfile(0.35f);
        lexoniaStationBeta.setCustomDescriptionId("kiprad_lexonia_station_beta");
        lexoniaStationBeta.setInteractionImage("illustrations", "abandoned_station2");
        lexoniaStationBeta.getMemoryWithoutUpdate().set("$abandonedStation", true);

         */


        ///jump points///

        //dell custom jump point
        JumpPointAPI dellUniqueJumpPoint = Global.getFactory().createJumpPoint("kiprad_dell_jumpPoint", "Dellian Bridge");
        OrbitAPI dellUniqueJumpPointOrbit = Global.getFactory().createCircularOrbit(lexoniaDell, 90f, 550f, 25f);
        dellUniqueJumpPoint.setOrbit(dellUniqueJumpPointOrbit);
        dellUniqueJumpPoint.setRelatedPlanet(lexoniaDell);
        dellUniqueJumpPoint.setStandardWormholeToHyperspaceVisual();
        lexoniaSystem.addEntity(dellUniqueJumpPoint);

        //atrasti custom jump point
        JumpPointAPI atrastiUniqueJumpPoint = Global.getFactory().createJumpPoint("kiprad_atrasti_jumpPoint", "One Eyed Causeway");
        OrbitAPI atrastiUniqueJumpPointOrbit = Global.getFactory().createCircularOrbit(lexoniaAtrasti, 90f, 800f, 50f);
        atrastiUniqueJumpPoint.setOrbit(atrastiUniqueJumpPointOrbit);
        atrastiUniqueJumpPoint.setRelatedPlanet(lexoniaAtrasti);
        atrastiUniqueJumpPoint.setStandardWormholeToHyperspaceVisual();
        lexoniaSystem.addEntity(atrastiUniqueJumpPoint);

        //add orbiting entities beyond the custom planets
        float radiusAfterSecondOrbitingEntities = StarSystemGenerator.addOrbitingEntities(lexoniaSystem, lexoniaStar, StarAge.AVERAGE, 2, 4, lexoniaStilleta.getCircularOrbitRadius(), 2, true);

        //autogenerate jump points in hyperspace and in system
        lexoniaSystem.autogenerateHyperspaceJumpPoints(true, true);

        //set up hyperspace editor plugin
        HyperspaceTerrainPlugin hyperspaceTerrainPlugin = (HyperspaceTerrainPlugin) Misc.getHyperspaceTerrain().getPlugin();
        NebulaEditor nebulaEditor = new NebulaEditor(hyperspaceTerrainPlugin);

        //set up radius's in hyperspace of system
        float minHyperspaceRadius = hyperspaceTerrainPlugin.getTileSize() * 2f;
        float maxHyperspaceRadius = lexoniaSystem.getMaxRadiusInHyperspace();

        //hyperstorm-b-gone (around system in hyperspace)
        nebulaEditor.clearArc(lexoniaSystem.getLocation().x, lexoniaSystem.getLocation().y, 0, minHyperspaceRadius + maxHyperspaceRadius, 0f, 360f, 0.25f);
    }
}
