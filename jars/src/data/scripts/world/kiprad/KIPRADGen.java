package data.scripts.world.kiprad;

import com.fs.starfarer.api.campaign.FactionAPI;
import com.fs.starfarer.api.campaign.RepLevel;
import com.fs.starfarer.api.campaign.SectorAPI;
import com.fs.starfarer.api.campaign.SectorGeneratorPlugin;
import com.fs.starfarer.api.impl.campaign.ids.Factions;
import com.fs.starfarer.api.impl.campaign.shared.SharedData;
import data.scripts.world.kiprad.lexonia.Lexonia;

public class KIPRADGen implements SectorGeneratorPlugin {

    public static void initFactionRelationships(SectorAPI sector) {
        FactionAPI hegemony = sector.getFaction(Factions.HEGEMONY);
        FactionAPI tritachyon = sector.getFaction(Factions.TRITACHYON);
        FactionAPI pirates = sector.getFaction(Factions.PIRATES);
        FactionAPI church = sector.getFaction(Factions.LUDDIC_CHURCH);
        FactionAPI path = sector.getFaction(Factions.LUDDIC_PATH);
        FactionAPI league = sector.getFaction(Factions.PERSEAN);
        FactionAPI kiprad = sector.getFaction("kiprad");

        kiprad.setRelationship(path.getId(), RepLevel.HOSTILE);
        kiprad.setRelationship(hegemony.getId(), RepLevel.HOSTILE);
        kiprad.setRelationship(pirates.getId(), RepLevel.HOSTILE);
        kiprad.setRelationship(tritachyon.getId(), RepLevel.NEUTRAL);
        kiprad.setRelationship(league.getId(), RepLevel.FRIENDLY);
        kiprad.setRelationship(church.getId(), RepLevel.SUSPICIOUS);
    }

    @Override
    public void generate(SectorAPI sector) {
        SharedData.getData().getPersonBountyEventData().addParticipatingFaction("kiprad");

        initFactionRelationships(sector);

        new Lexonia().generate(sector);
    }
}